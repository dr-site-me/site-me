export class Category {
  id: string;
  name: string;
  parentCategoryId: string;
}
