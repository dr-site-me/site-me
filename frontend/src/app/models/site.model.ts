export class Site {
  author: string;
  id: string;
  name: string;
  description: string;
  url: string;
}
