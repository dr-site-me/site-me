module.exports = {
    tabWidth: 4,
    useTabs: false,
    printWidth: 100,
    semi: true,
    singleQuote: false,
    bracketSpacing: false,
    jsxBracketSameLine: false,
    arrowParens: "avoid"
};
